const url = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?";
const apiKey = "dict.1.1.20181102T204804Z.2a788d22b983934d.79420de7ab27e7dadb1e7e866ae2b35fa4f47a28";

const parseData = (def) => def && def.length ?
  def.reduce((acc, { pos, ts, tr }) => {
    return [...acc, tr.reduce((acc, { ex, mean }) => {
      return {
        ...acc,
        examples: [...acc.examples, ex ? ex.reduce((exs, { text }) => {
          return [...exs, text];
        }, []) : []].reduce((vals, arr) => {
          return [...vals.concat(arr)];
        }, []),
        synonyms: [...acc.synonyms, mean ? mean.reduce((means, { text }) => {
          return [...means, text];
        }, []) : []].reduce((vals, arr) => {
          return [...vals.concat(arr)];
        }, []),
      }
    }, {
        pos: pos,
        transcription: ts,
        examples: [],
        synonyms: []
      })];
  }, [])
  : null;

export const requestApi = async (word) => {
  return await fetch(`${url}key=${apiKey}&lang=en-ru&text=${word}`, {
    method: 'GET'
  })
    .then(res => res.json())
    .then(json => parseData(json.def))
    .catch(err => { console.log(err) })
}