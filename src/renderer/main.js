import Vue from 'vue';
import Vuex from 'vuex';

import App from './App';

import '../reset/reset.css';

Vue.config.productionTip = false;

import { store } from './store/index.js';

/* eslint-disable no-new */
new Vue({
  components: { App },
  store,
  template: '<App/>'
}).$mount('#app');
