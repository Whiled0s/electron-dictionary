import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    current: {
      word: '...',
      info: null
    },
    history: []
  },
  mutations: {
    REPLACE_CURRENT(state, { word, info }) {
      state.current = {
        word,
        info
      }
    },
    ADD_TO_HISTORY(state, payload) {
      state.history = [payload, ...state.history];
    },
    REMOVE_FROM_HISTORY(state, index) {
      let copy = state.history;
      copy.splice(index, 1);
      state.history = copy;
    }
  },
  getters: {
    currentWord: state => {
      const word = state.current.word;
      return word === '' ? '...' : word;
    },
    currentInfo: state => state.current.info,
    history: state => state.history
  }
})